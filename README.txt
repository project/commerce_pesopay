Module: Commerce PesoPay http://www.pesopay.com/
Author: Anil Sagar
Sponsored By: Apigee http://www.apigee.com


Description
===========
Integrates PesoPay as a payment gateway for Drupal Commerce. Allows transactions 
to be made using Phillipine Peso's.


Requirements
============
Commerce


Installation
============
Copy the 'commerce_pesopay' module directory in to your Drupal
sites/all/modules directory as usual

Enable the module from the Modules -> Commerce PesoPay section


Setup
=====

1. Change Currency settings in admin/commerce/config/currency
2. Select default store currency to PHP - Phillipine Peso
3. Enable Currency PHP and make sure you disable USD
4. Goto 
   admin/commerce/config/payment-methods
5. Click on Edit Operations
6. Click on Edit Action Next to Enable Payment Method: Commerce Pesopay
5. Configure Merchant ID and Secret Key
6. Choose Test or Live Transaction mode
7. Configure Redirect URI in peso pay to 
   yoursite.com/commerce/payment-capture/pesopay/datafeed
